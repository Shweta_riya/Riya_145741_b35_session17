<?php
class StudentInfo{
    public $justonevariable="hello bitm";
    public function justonemethod()
    {
        echo "hello world!";
    }

    public function __construct()
    {
        echo $this->justonevariable;
    }
    public function __destruct()
    {
        echo "gd bye";
    }

}

$justoneobject=new StudentInfo;
unset($justoneobject);

$justoneobject->justonemethod();
