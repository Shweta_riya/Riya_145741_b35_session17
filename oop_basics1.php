<?php
 class StudentInfo{
     public $std_id="";
     public $std_name="";
     public $std_cgpa=0.00;
     public function set_std_id($std_id){
         $this->std_id=$std_id;
         //echo $this->std_id;
     }
     public function set_std_name($std_name){
         $this->std_name=$std_name;
         //echo $this->std_id;
     }
     public function set_std_cgpa($std_cgpa){
         $this->std_cgpa=$std_cgpa;
         //echo $this->std_id;
     }
     public function get_std_id(){
         return $this->std_id;
     }
     public function get_std_name(){
         return $this->std_name;
     }
     public function get_std_cgpa(){
         return $this->std_cgpa;
     }
     public function showDetails(){
         echo "Student id:" . $this->std_id."<br>";
         echo "Student name:" . $this->std_name."<br>";
         echo "Student cgpa:" . $this->std_cgpa."<br>";
     }

 }
$obj= new StudentInfo;
$obj->set_std_id("seip1234");
$obj->set_std_name("seip");
$obj->set_std_cgpa(3.96);
$obj->showDetails();
//echo $obj->get_std_id()."<br>";
//echo $obj->get_std_name()."<br>";
//echo $obj->get_std_cgpa();

